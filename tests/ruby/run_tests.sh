#!/bin/bash
DIR="$(cd "$(dirname "$BASH_SOURCE[0]")" && pwd)"

testAwsCli() {
    VERSION=`aws --version 2>&1`
    assertEquals "AWS CLI Cmd" 0 $?
    assertNotEquals "AWS CLI Version" "" "$VERSION"
}

testRuby() {
    VERSION=`env ruby --version`
    assertEquals "Ruby Cmd" 0 $?
    assertNotEquals "Ruby Version" "" "$VERSION"
}

testBundle() {
    VERSION=`bundle --version`
    assertEquals "Bundle Cmd" 0 $?
    assertNotEquals "Bundle Version" "" "$VERSION"
}

testSSH() {
    VERSION=`which ssh-agent`
    assertEquals "ssh-agent CMD" 0 $?
    VERSION=`which ssh-add`
    assertEquals "ssh-add CMD" 0 $?
}

testRubyHeaders() {
    FOUND=`find /usr/ -name ruby.h`
    assertNotEquals "Found Ruby Headers" "" "$FOUND"
}

testInstallGemWithNativeExtensions() {
    gem install json
    assertEquals "gem install CMD" 0 $?
}

testInstallFastlane() {
    gem install fastlane
    assertEquals "gem install CMD" 0 $?
}

testInstallCocoapods() {
    gem install cocoapods
    assertEquals "gem cocoapods CMD" 0 $?
}

testTs() {
    TS=`echo a | ts -s`
    assertEquals "ts test" "00:00:00 a" "$TS"
}

# Load shUnit2.
. $DIR/shunit2