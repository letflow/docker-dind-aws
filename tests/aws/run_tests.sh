#!/bin/bash
DIR="$(cd "$(dirname "$BASH_SOURCE[0]")" && pwd)"

testAwsCli() {
    VERSION=`aws --version 2>&1`
    assertEquals "AWS CLI Cmd" 0 $?
    assertNotEquals "AWS CLI Version" "" "$VERSION"
}

testSSH() {
    VERSION=`which ssh-agent`
    assertEquals "ssh-agent CMD" 0 $?
    VERSION=`which ssh-add`
    assertEquals "ssh-add CMD" 0 $?
}

testTs() {
    TS=`echo a | ts -s`
    assertEquals "ts test" "00:00:00 a" "$TS"
}

# Load shUnit2.
. $DIR/shunit2