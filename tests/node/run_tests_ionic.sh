#!/bin/bash
DIR="$(cd "$(dirname "$BASH_SOURCE[0]")" && pwd)"

testNode() {
    VERSION=`node --version`
    assertEquals "node.js CMD" 0 $?
    assertNotEquals "node.js Version" "" "$VERSION"
}

testAwsCli() {
    VERSION=`aws --version 2>&1`
    assertEquals "AWS CLI CMD" 0 $?
    assertNotEquals "AWS CLI Version" "" "$VERSION"
}

testGit() {
    VERSION=`git --version`
    assertEquals "git CMD" 0 $?
    assertNotEquals "git Version" "" "$VERSION"
}

testGradle() {
    VERSION=`gradle --version`
    assertEquals "gradle CMD" 0 $?
    assertNotEquals "gradle Version" "" "$VERSION"
}

testSdkManager() {
    VERSION=`sdkmanager --version`
    assertEquals "sdkmanager CMD" 0 $?
    assertNotEquals "sdkmanager Version" "" "$VERSION"
}

testJavac() {
    VERSION=`javac -version 2>&1`
    assertEquals "javac CMD" 0 $?
    assertEquals "javac Version" "11.0." ${VERSION:6:5}
}

testJarsigner() {
    VERSION=`which jarsigner`
    assertEquals "jarsigner CMD" 0 $?
}

testKeytool() {
    VERSION=`which keytool`
    assertEquals "keytool CMD" 0 $?
}

testZipAlign() {
    VERSION=`which zipalign`
    assertEquals "zipalign CMD" 0 $?
}

testXmlStarlet() {
    VERSION=`which xmlstarlet`
    assertEquals "xmlstarlet CMD" 0 $?
}

testSSH() {
    VERSION=`which ssh-agent`
    assertEquals "ssh-agent CMD" 0 $?
    VERSION=`which ssh-add`
    assertEquals "ssh-add CMD" 0 $?
}

testPython() {
    VERSION=`python3 --version 2>&1`
    assertEquals "python3 CMD" 0 $?
    assertNotEquals "python3 Version" "" "$VERSION"
}

testBuildTools() {
    VERSION=`make --version 2>&1`
    assertEquals "make CMD" 0 $?
    assertNotEquals "make Version" "" "$VERSION"

    VERSION=`g++ --version 2>&1`
    assertEquals "g++ CMD" 0 $?
    assertNotEquals "g++ Version" "" "$VERSION"

    VERSION=`gcc --version 2>&1`
    assertEquals "gcc CMD" 0 $?
    assertNotEquals "gcc Version" "" "$VERSION"
}

testTs() {
    TS=`echo a | ts -s`
    assertEquals "ts test" "00:00:00 a" "$TS"
}

testIonic() {
    ionic -v && cordova -v --no-telemetry && cordova-res -v
}



# Load shUnit2.
. $DIR/shunit2