#!/bin/bash
DIR="$(cd "$(dirname "$BASH_SOURCE[0]")" && pwd)"

DOCKER_TEST_IMAGE=$1

PHP_TEST_IMAGE="letflow/php:7.4-latest"
PHP_CONTAINER=$(docker run --detach --rm $PHP_TEST_IMAGE)

CONTAINER=$(docker run --detach --rm -v $PWD/tests/nginx:/var/opt/tests --link $PHP_CONTAINER:app $DOCKER_TEST_IMAGE)
docker exec -it $CONTAINER sh
docker stop $CONTAINER
docker stop $PHP_CONTAINER