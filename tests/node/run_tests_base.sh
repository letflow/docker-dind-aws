#!/bin/bash
DIR="$(cd "$(dirname "$BASH_SOURCE[0]")" && pwd)"

testNode() {
    VERSION=`node --version`
    assertEquals "node.js CMD" 0 $?
    assertNotEquals "node.js Version" "" "$VERSION"

    VERSION=`npm --version`
    assertEquals "npm CMD" 0 $?
    assertNotEquals "npm Version" "" "$VERSION"
}

testAwsCli() {
    VERSION=`aws --version 2>&1`
    assertEquals "AWS CLI CMD" 0 $?
    assertNotEquals "AWS CLI Version" "" "$VERSION"
}

testGit() {
    VERSION=`git --version`
    assertEquals "git CMD" 0 $?
    assertNotEquals "git Version" "" "$VERSION"
}

testXmlStarlet() {
    VERSION=`which xmlstarlet`
    assertEquals "xmlstarlet CMD" 0 $?
}

testSSH() {
    VERSION=`which ssh-agent`
    assertEquals "ssh-agent CMD" 0 $?
    VERSION=`which ssh-add`
    assertEquals "ssh-add CMD" 0 $?
}

testPython() {
    VERSION=`python3 --version 2>&1`
    assertEquals "python3 CMD" 0 $?
    assertNotEquals "python3 Version" "" "$VERSION"
}

testBuildTools() {
    VERSION=`make --version 2>&1`
    assertEquals "make CMD" 0 $?
    assertNotEquals "make Version" "" "$VERSION"

    VERSION=`g++ --version 2>&1`
    assertEquals "g++ CMD" 0 $?
    assertNotEquals "g++ Version" "" "$VERSION"

    VERSION=`gcc --version 2>&1`
    assertEquals "gcc CMD" 0 $?
    assertNotEquals "gcc Version" "" "$VERSION"
}

testTs() {
    TS=`echo a | ts -s`
    assertEquals "ts test" "00:00:00 a" "$TS"
}

# Load shUnit2.
. $DIR/shunit2