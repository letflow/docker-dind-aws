#!/bin/bash
DIR="$(cd "$(dirname "$BASH_SOURCE[0]")" && pwd)"

DOCKER_TEST_IMAGE=$1

docker run --rm $DOCKER_TEST_IMAGE /var/opt/tests/run_tests.sh