#!/bin/bash
DIR="$(cd "$(dirname "$BASH_SOURCE[0]")" && pwd)"

testDockerCompose() {
    VERSION=`docker-compose --version`
    assertEquals "Docker Compose Cmd" 0 $?
    assertNotEquals "Docker Compose Version" "" ${VERSION:22:7}
}

testAwsCli() {
    VERSION=`aws --version 2>&1`
    assertEquals "AWS CLI Cmd" 0 $?
    assertNotEquals "AWS CLI Version" "" "$VERSION"
}

testEcsCli() {
    VERSION=`ecs-cli --version`
    assertEquals "ECS CLI Cmd" 0 $?
    assertNotEquals "ECS CLI Version" "" "$VERSION"
}

testSSH() {
    VERSION=`which ssh-agent`
    assertEquals "ssh-agent CMD" 0 $?
    VERSION=`which ssh-add`
    assertEquals "ssh-add CMD" 0 $?
}

testJq() {
    VERSION=`jq --version`
    assertEquals "jq CMD" 0 $?
    assertNotEquals "jq Version" "" "$VERSION"
}

testYq() {
    VERSION=`yq --version`
    assertEquals "yq CMD" 0 $?
    assertNotEquals "yq Version" "" "$VERSION"
}

testTs() {
    TS=`echo a | ts -s`
    assertEquals "ts test" "00:00:00 a" "$TS"
}


# Load shUnit2.
. $DIR/shunit2