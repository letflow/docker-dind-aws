Docker DIND with AWS tools
===

The image is partially based on [tmaier/docker-compose](https://github.com/tmaier/docker-compose)
The image is developed and maintained by [LetFlow](http://www.letflow.com.ar)

GitLab: https://gitlab.com/letflow/docker-dind-aws

Features:
* Based on docker:latest
* Include AWS CLI
* Include AWS ECS CLI
* Suitable to be used in CI

## Usage instructions for GitLab CI

You may use it like this in your `.gitlab-ci.yml` file.

```yaml
image: letflow/docker-dind-aws:compose-latest

services:
  - docker:dind

before_script:
  - $(aws ecr get-login --no-include-email)

build:
  stage: build
  script:
    - docker-compose build
    - docker push $DOCKER_TEST_IMAGE

staging:
  stage: staging_app
  script:
    - ecs-cli compose --file docker-compose.yml --project-name $CI_PROJECT_PATH_SLUG-$CI_COMMIT_REF_SLUG --cluster $ECS_CLUSTER --ecs-params ecs-params.yml service up
```
