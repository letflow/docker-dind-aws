#!/bin/bash
DIR="$(cd "$(dirname "$BASH_SOURCE[0]")" && pwd)"

NAME=$(basename $DIR)

TYPE=${1:-node}
VERSION=${2:-latest}
VARIANT=${3:-base}
VARIANT_VERSION=${4:-latest}


##############
### Colors ###
##############
ecolor='\e[0m'
# Normal #
blue='\e[0;34m'
green='\e[0;32m'
red='\e[0;31m'
yellow='\e[0;33m'
# Bright #
bblue='\e[1;34m'
bgreen='\e[1;32m'
bred='\e[1;31m'
byellow='\e[1;33m'
##################
### End Colors ###
##################

case "$VARIANT" in
 base) TAG=$VERSION ;;
 *) TAG="$VERSION-$VARIANT-$VARIANT_VERSION" ;;
esac

IMAGE="$NAME-$TYPE"
DOCKER_BUILD_IMAGE="$IMAGE:$TAG"
DOCKERFILE="images/$TYPE/Dockerfile-$VARIANT"
if [ ! -e $DOCKERFILE ]; then
    DOCKERFILE="images/$TYPE/Dockerfile"
fi

echo -e "Building tag ${yellow}$TAG${ecolor} and image ${yellow}$DOCKER_BUILD_IMAGE${ecolor} from ${yellow}$DOCKERFILE${ecolor} stage ${yellow}$VARIANT${ecolor}"
docker build --tag $DOCKER_BUILD_IMAGE --file $DOCKERFILE --build-arg VERSION=$VERSION --build-arg VARIANT_VERSION=$VARIANT_VERSION --target=$VARIANT . | ts -s
