#!/bin/bash
DIR="$(cd "$(dirname "$BASH_SOURCE[0]")" && pwd)"

NAME=$(basename $DIR)

TYPE=${1:-node}
VERSION=${2:-latest}
VARIANT=${3:-base}
VARIANT_VERSION=${4:-latest}

case "$VARIANT" in
 base) TAG=$VERSION ;;
 *) TAG="$VERSION-$VARIANT-$VARIANT_VERSION" ;;
esac

IMAGE="$NAME-$TYPE"
DOCKER_BUILD_IMAGE="$IMAGE:$TAG"

echo "Entering image $DOCKER_BUILD_IMAGE"
docker run -it --rm $DOCKER_BUILD_IMAGE sh